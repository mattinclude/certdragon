﻿'use strict';

app.directive('aqFocus', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {

            $timeout(function () {
                element[0].focus();
            }, 100);

        }
    }
}]);

app.directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            var defaultSrc = attrs.src;
            element.bind('error', function () {
                if (attrs.errSrc) {
                    element.attr('src', attrs.errSrc);
                }
                else if (attrs.src) {
                    element.attr('src', defaultSrc);
                }
                element.parent().removeClass("wait-bg");
            });
        }
    }
});

app.directive('errFunc', function () {
    return {
        scope: {
            errFunc: '&'
        },
        link: function (scope, element, attrs) {
            //var defaultSrc = attrs.src;
            element.bind('error', function () {
                scope.errFunc();
            });
        }
    }
});

app.directive('htmlContent', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: "/app/html/directives/htmlcontent.html",
        scope: {
            htmlContent: '='
        },
        link: function (scope, element, attributes) {

            //scope.html = scope.htmlContent;
            //$compile(scope.htmlContent)(scope);

            setTimeout(function () {
                $('[data-toggle="tooltip"]').tooltip();
            }, 1000);

        }
    }   
}]);





