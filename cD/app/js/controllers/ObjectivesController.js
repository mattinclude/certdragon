﻿angular.module('app')
  .controller('ObjectivesCtrl', ['$state',
      function ($state) {

          var obj = this;

          obj.loading = true;          
          
          var quiz = $state.params.quiz;
          var part = $state.params.part;

          if (quiz === null) {
              $state.transitionTo('menu');
              return;
          }

          if (part === null) {
              $state.transitionTo('menu', { quiz: quiz });
              return;
          }

          var objectives = part.objectives;
          console.log(part.objectives);

          //_.each(objectives, function (objective) {
          //    //update contentfile prop to use template folder path
          //    objective.contentfile = templatePath + objective.contentfile;       
          //});

          obj.objectives = objectives;

          obj.loading = false;

      }]);

