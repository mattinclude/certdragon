
angular.module('app')
  .controller('QuizCtrl', ['$scope', 'QUIZ_CONFIG', 'QuizService', '$timeout', '$state', 'AudioService',
      function ($scope, QUIZ_CONFIG, QuizService, $timeout, $state, AudioService) {

          var quiz = this;
          var thequiz = $state.params.quiz;
          var part = $state.params.part;

          console.log($state.params);

          if (thequiz == null || part == null) {
              $state.transitionTo('menu');
              return;
          }

          QuizService.InitializeQuiz(thequiz, part);

          if (!thequiz) { //quiz not found
              alert("This quiz was not found or has no questions");
              $state.transitionTo('menu');
              return;
          }

          quiz.question = {};
          quiz.selectedChoices = [];
          quiz.questionCount = QuizService.QuestionCount;
          quiz.currentQuestion = 1;
          quiz.quizStarted = false;
          quiz.selectedChoices = [];
          quiz.canSubmit = false;
          quiz.answered = false;
          quiz.correct = false;
          quiz.incorrect = false;
          quiz.quizComplete = false;
          quiz.nextQuestion = false;
          quiz.userAnswer = null;
          quiz.userAnswerInvalid = false;

          var startQuiz = function () {

              var message = "good luck, you have " + QUIZ_CONFIG.QUIZLENGTH + " minutes, and " + QuizService.QuestionCount + " questions";
              toastr.options.onHidden = function () {


                  //as we're running from a jquery based function
                  $timeout(function () {
                      timer();
                      createQuestion();

                      //reset toastr
                      toastr.options.onHidden = null;

                  }, 0);

              };

              toastr.options.progressBar = true;
              toastr.success(message, 'Begin Quiz', { positionClass: "toast-top-center" });
          };

          var createQuestion = function (a) {

              quiz.quizStarted = true;
              quiz.canSubmit = false;
              quiz.answered = false;
              quiz.correct = false;
              quiz.incorrect = false;
              quiz.quizComplete = false;
              quiz.nextQuestion = false;

              quiz.question = QuizService.GetNextQuestion();
              quiz.questionCount = QuizService.QuestionCount;
              quiz.currentQuestion = QuizService.CurrentQuestionIndex + 1;
              quiz.question.id = quiz.currentQuestion;

              quiz.selectedChoices = [];

              angular.forEach(quiz.question.choices, function (choice, index) {
                  var c = {
                      id: index,
                      text: choice,
                      selected: false,
                      correct: false,
                      incorrect: false
                  };

                  this.push(c);

              }, quiz.selectedChoices);

              setQuestionIsMultipleChoice();

              if (!quiz.question.isMultiChoice) {
                  quiz.userAnswer = null; //reset
                  $scope.$watch(function () {
                      return quiz.userAnswer;
                  }, function (n, o) {

                      if (n !== null) {
                          quiz.canSubmit = n.length > 0; //allow user to enter and submit manually
                          if (n.length === 0) {
                              quiz.userAnswerInvalid = true;
                          }
                      }
                      else {
                          quiz.canSubmit = false;
                      }

                  });

              }

          };

          var setQuestionIsMultipleChoice = function () {

              quiz.question.isMultiChoice = quiz.question.choices.length > 0; // $.isArray(quiz.question["correct"]);
              return quiz.question.isMultiChoice;
          }

          quiz.selectChoice = function (index) {

              if (quiz.answered) {
                  return;
              }

              if (QUIZ_CONFIG.MAXSELECTIONS == quiz.getSelections().length) {
                  if (quiz.selectedChoices[index].selected) {
                      quiz.selectedChoices[index].selected = false;
                  }
              }
              else {
                  //toggle the selection
                  quiz.selectedChoices[index].selected = !quiz.selectedChoices[index].selected;
              }

              quiz.canSubmit = quiz.getSelections().length > 0;
          };

          quiz.getSelections = function () {

              var selected = _.filter(quiz.selectedChoices, function (choice) { return choice.selected; });
              return selected;
          };

          quiz.submitAnswer = function () {

              if (quiz.canSubmit) {

                  AudioService.Click();

                  if (!quiz.question.isMultiChoice) {
                      submitUserAnswer();
                  }
                  else {

                      var selections = quiz.getSelections();

                      if (selections.length > 0) {

                          quiz.answered = true;

                          var answers = [];
                          _.each(selections, function (answer, index) {
                              answers.push(answer.text);
                          });

                          //if (arraysAreIdentical(answers, quiz.question["correct"])) {
                          if (compareAnswers(quiz.question["correct"], answers) == true) {

                              quiz.correct = true;

                              _.each(selections, function (answer, index) {
                                  answer.correct = true;
                              });

                              QuizService.Score++;

                          }
                          else {

                              quiz.incorrect = true;

                              _.each(selections, function (answer, index) {
                                  answer.incorrect = true;
                              });

                              QuizService.MissedQuestions.push(quiz.question);

                          }

                      }
                      else {
                          return; // nothing selected so bail
                      }

                  }

                  if (quiz.currentQuestion == quiz.questionCount) {

                      quiz.quizComplete = true;
                      $scope.$emit('quiz-complete');
                  }
                  else {
                      quiz.nextQuestion = true;

                  }

              }

          };

          var submitUserAnswer = function () {

              quiz.answered = true;
              var cleaned = quiz.userAnswer.replace(/\s\s+/g, '');
              //var answer = quiz.question["correct"][0]; //only use first answer
              var answers = quiz.question["correct"];

              cleaned = cleaned.toUpperCase();

              for (var i = 0, len = answers.length; i < len; i++) {

                  var answer = answers[i].toUpperCase();
                  var areEqual = cleaned === answer;

                  if (areEqual) {
                      quiz.correct = true;
                      QuizService.Score++;
                      break;
                  }

              }

              if (quiz.correct === true) {
                  //nada
              }
              else {
                  quiz.incorrect = true;
                  QuizService.MissedQuestions.push(quiz.question);
              }
              //answer = answer.toUpperCase();

              //var areEqual = cleaned === answer;

              //if (areEqual) {

              //    quiz.correct = true;
              //    QuizService.Score++;

              //}
              //else {

              //    quiz.incorrect = true;
              //    QuizService.MissedQuestions.push(quiz.question);

              //}

          };

          quiz.gotoNextQuestion = function () {
              AudioService.Click();
              $("html, body").animate({ scrollTop: 0 }, 200);
              createQuestion();
          };

          quiz.finishQuiz = function () {
              AudioService.Click();
              $state.transitionTo('results', { quiz: thequiz, part: part });
          };

          var arraysAreIdentical = function (arr1, arr2) {

              if (arr1.length !== arr2.length) return false;
              for (var i = 0, len = arr1.length; i < len; i++) {
                  if (arr1[i] !== arr2[i]) {
                      return false;
                  }
              }
              return true;
          }

          var compareAnswers = function (choices, answers) {

              if (choices.length !== answers.length) return false;

              var correct = _.every(choices, function (choice) {

                  var found = _.find(answers, function (answer)
                  {
                      return answer == choice;
                  });

                  if (found == undefined) {
                      return false;
                  }
                  else {
                      return true;
                  }

              });

              return correct;
          }

          var runTimer = true;
          var timerinstance;
          var timer = function () {

              timerinstance = $timeout(function () {

                  if (runTimer) {
                      var time = moment();
                      $scope.$emit('timer-tick', { time: time });
                      timer();
                  }

              }, 1000, true);

          };

          $scope.$on('timer-done', function () {
              runTimer = false;
          });

          $scope.$on("$destroy", function (event) {
              $timeout.cancel(timerinstance);
              }
          );

          $scope.$on('quiz-cancelled', function (event, args) {
              runTimer = false;
              $timeout.cancel(timerinstance);
          });

          //run the app
          startQuiz();


      }]);
