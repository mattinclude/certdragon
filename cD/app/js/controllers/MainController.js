﻿angular.module('app')
  .controller('MainCtrl', ['$scope', '$timeout', 'QUIZ_CONFIG', 'QuizService', '$state', 'AudioService',
      function ($scope, $timeout, QUIZ_CONFIG, QuizService, $state, AudioService) {
          
          var main = this;          

          main.showCountdown = true;
          main.showTitle = true;

          main.quizTitle = QUIZ_CONFIG.QUIZTITLE;         
          main.quizstarted = false;
          main.splash = false;         

          $scope.$watch('$state.current.data', function (newValue, oldValue) {

              if (newValue !== undefined) {                 

                  if ($state.current.name == "quiz") {
                      main.quizstarted = true;
                  }

                  main.title = newValue.title;

                  if (newValue.showCountdown !== undefined) {
                      main.showCountdown = newValue.showCountdown;                      
                  }
                  else {
                      main.showCountdown = true;
                  }

                  if (newValue.showTitle !== undefined) {                      
                      main.showTitle = newValue.showTitle;
                  }
                  else {
                      main.showTitle = true;
                  }

                  if (newValue.showBgImage !== undefined) {
                      main.showBgImage = newValue.showBgImage;
                  }
                  else {
                      main.showBgImage = false;
                  }
                  
              }
              
          });
          
          main.duration = moment.duration(QUIZ_CONFIG.QUIZLENGTH, 'minutes');
          var totalSeconds = QUIZ_CONFIG.QUIZLENGTH * 60;

          main.secondsleft = main.duration.format("hh:mm:ss");

          main.gotomenu = function () {

              AudioService.Click();

              main.quizstarted = false;
              main.duration = moment.duration(QUIZ_CONFIG.QUIZLENGTH, 'minutes');
              main.secondsleft = main.duration.format("hh:mm:ss");
              totalSeconds = QUIZ_CONFIG.QUIZLENGTH * 60;

              $timeout(function () {                  
                  $state.transitionTo('menu', { quiz: main.selectedQuiz, part: main.selectedQuizPart });
              }, 250);

          };

          $scope.$on('timer-tick', function (event, args) {              

              if (totalSeconds < 1) {
                  main.secondsleft = "";
                  return;
              }

              main.quizstarted = true;
              main.duration.subtract(1, 'second');
              totalSeconds--;

              if (totalSeconds < 1) {
                  toastr.error('The quiz time limit was reached.', 'Time is Up', { positionClass: "toast-top-center" });
                  $scope.$broadcast('timer-done');
                  main.secondsleft = "time is up";
              }
              else {
                  main.secondsleft = main.duration.format("hh:mm:ss");
              }             

          });

          $scope.$on('quiz-complete', function (event, args) {
              totalSeconds = 0;
              main.secondsleft = "";
          });
         
          $scope.$on('show-splash', function (event, args) {

              main.splash = true;                      

              $timeout(function () {  

                  $("#blackout").fadeOut(700, function () {
                      main.splash = false;
                  });                  

              }, QUIZ_CONFIG.SPLASHDURATION * 1000);

          });

          main.closeSplash = function () {

              $("#blackout").fadeOut(700, function () {
                  main.splash = false;
                  AudioService.StartBgMusic();
              });             
              
          };

          main.selectedQuiz = null;
          main.selectedQuizPart = null;

          $scope.$on('selected-quiz', function (event, args) {              
              main.selectedQuiz = args.quiz;
              main.selectedQuizPart = args.part;
          });

          $timeout(function () {
              $("#body").css("height", $(document).height() + 50);
              $("#blackout").css("height", $(document).height() + 50);
          }, 200);         


      }]);

