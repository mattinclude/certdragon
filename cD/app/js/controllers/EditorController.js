﻿angular.module('app')
  .controller('EditorCtrl', ['$timeout', '$scope', 'FileService','$q',
      function ($timeout, $scope, FileService, $q) {
          
          var editor = this;
          
          editor.files = [];
          editor.loading = false;
          editor.filename = '';
          editor.editing = false;
          editor.fileedit = {};

          editor.load = function () {

              editor.loading = true;
              editor.editing = false;

              FileService.QuizFiles.GetExamFiles().then(function (response) {
                  console.log(response);
                  if (response.status === 200) {

                      $(response.data).find("a").each(function () {
                           
                            var href = $(this).attr("href");
                            if (href.indexOf(".js") > -1) {
                                editor.files.push($(this).attr("href"));
                            }

                        });

                  }
                  else {
                      alert('error loading files! - ' + response.toString());
                      console.error("error loading files!", response);
                      
                  }

              }).finally(function () {
                  editor.loading = false;
              });            
              
          };

          editor.edit = function (index) {

              editor.editing = true;
              editor.filename = editor.files[index];

              FileService.QuizFiles.GetExamFile(editor.filename).then(function (response) {
                  console.log(response);
                  if (response.status === 200) {                     
                      editor.fileedit = response.data;
                  }
                  else {
                      editor.editing = true;
                      alert('error loading file! - ' + response.toString());
                      console.error("error loading file!", response);
                  }

              }).finally(function () {
                  
              });

          };

          editor.save = function () {

              if (editor.filename && editor.fileedit) {

                  FileService.QuizFiles.SaveExamFile(editor.filename, editor.fileedit);                  
                  
              }

          };



          editor.filenames = [];
          editor.fileobjs = FileService.QuizFileObjects; // [];
          FileService.QuizFiles.GetExamFileObjects();
          //editor.load();

          function one_step_promise() {
              var deferred = $q.defer();
              deferred.promise
              .then(function () {
                  
                  FileService.QuizFiles.GetExamFiles().then(function (response) {

                      if (response.status === 200) {

                          $(response.data).find("a").each(function () {

                              var href = $(this).attr("href");
                              if (href.indexOf(".js") > -1) {

                                  editor.filenames.push(href);
                              }

                          });

                      }
                      else {
                          alert('error loading files! - ' + response.toString());
                          console.error("error loading files!", response);

                      }

                  }).finally(function () {
                      console.log('1', editor.fileobjs);
                  });

              });
              $timeout(function () {
                  deferred.resolve();
              }, 500);
              return deferred.promise;
          }

          function two_step_promise() {

              var deferred = $q.defer();
              var promise = deferred.promise
              .then(function () {

                  angular.forEach(editor.filenames, function (filename) {

                      FileService.QuizFiles.GetExamFile(filename).then(function (response) {

                          if (response.status === 200) {
                              response.data.filepath = filename;
                              editor.fileobjs.push(response.data);
                              // console.log(editor.quizzes);
                          }
                          else {
                              alert('error loading file! - ' + response.toString());
                              console.error("error loading file!", response);
                          }

                      }).finally(function () {
                          //_.sortBy(editor.fileobjs, function (o) {
                          //    console.log(o.sortOrder);
                          //    return o.sortOrder;
                          //});
                          //console.log('2', editor.fileobjs);

                          var data = _(editor.fileobjs).sortBy("id").value();
                          console.log(data);

                          if (editor.fileobjs.length === editor.filenames.length) {

                              alert('done');

                              //JavaScript

                              //data = _(data).chain()
                              //  .sortBy('value')
                              //  .sortBy('type')
                              //  .reverse()
                              //  .value();
                              //1
                              //2
                              //3
                              //4
                              //5
                              //data = _(data).chain()
                              //  .sortBy('value')
                              //  .sortBy('type')
                              //  .reverse()
                              //  .value();

                              //_.sortBy(editor.fileobjs, function (o) {                                  
                              //    return -o.sortOrder;
                              //});

                              console.log('2', _.sortBy(editor.fileobjs, function (num) {

                                  return num.sortOrder;

                              }));

                          }

                      });

                  });

              });

              $timeout(function () {
                  deferred.resolve();
              }, 500);
              return promise;

          };


          var deferred = $q.defer();
          deferred.promise
              .then(function () {
                  console.log('first', editor.fileobjs);
                  //_.sortBy(editor.fileobjs, function (o) {
                  //    console.log(o.sortOrder);
                  //    return -o.sortOrder;
                  //});

              })
          .then(function () {
              
              return one_step_promise();

              //return FileService.QuizFiles.GetExamFiles().then(function (response) {

              //    if (response.status === 200) {

              //        $(response.data).find("a").each(function () {

              //            var href = $(this).attr("href");
              //            if (href.indexOf(".js") > -1) {

              //                editor.filenames.push(href);                              
              //            }

              //        });

              //    }
              //    else {
              //        alert('error loading files! - ' + response.toString());
              //        console.error("error loading files!", response);

              //    }

              //}).finally(function () {

              //});

          })
          .then(function () {

              //angular.forEach(editor.filenames, function (filename) {

              //    FileService.QuizFiles.GetExamFile(filename).then(function (response) {

              //        if (response.status === 200) {
              //            response.data.filepath = filename;
              //            editor.fileobjs.push(response.data);
              //            // console.log(editor.quizzes);
              //        }
              //        else {
              //            alert('error loading file! - ' + response.toString());
              //            console.error("error loading file!", response);
              //        }

              //    }).finally(function () {
              //        //console.log(editor.quizzes);
              //    });

              //});
              return two_step_promise();
              
          }).then(function () {
              console.log('last', editor.fileobjs);
              //_.sortBy(editor.fileobjs, function (o) {
              //    console.log(o.sortOrder);
              //    return -o.sortOrder;
              //});

          });

          //deferred.resolve();



          //$scope.next = function () {
          //    return FileService.QuizFiles.GetExamFileObjects().then(function (response) {
          //        console.log(response);
          //    }).then(function (question) {
          //        //$scope.question = question;
          //    });
          //};

          //$scope.$watch(function () {
          //    return $scope.next;
          //}, function (n, o) {
          //    console.log(n);
          //});

          ////$scope.next();
          //console.log($scope.next());
          //$timeout(function () {          

          //FileService.QuizFiles.GetExamFiles().then(function (response) {
              
          //    if (response.status === 200) {

          //        $(response.data).find("a").each(function () {

          //            var href = $(this).attr("href");
          //            if (href.indexOf(".js") > -1) {

          //                FileService.QuizFiles.GetExamFile(href).then(function (response) {

          //                    if (response.status === 200) {
          //                        response.data.filepath = href;
          //                        editor.quizzes.push(response.data);
          //                        console.log(editor.quizzes);
          //                    }
          //                    else {
          //                        alert('error loading file! - ' + response.toString());
          //                        console.error("error loading file!", response);
          //                    }

          //                }).finally(function () {                              
          //                    //console.log(editor.quizzes);
          //                });
          //            }

          //        });

          //    }
          //    else {
          //        alert('error loading files! - ' + response.toString());
          //        console.error("error loading files!", response);

          //    }

          //}).finally(function () {
             
          //});

          //}, 0);

      }]);

