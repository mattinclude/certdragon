﻿angular.module('app')
  .controller('ResultsCtrl', ['QuizService', 'QUIZ_CONFIG', '$state', '$timeout', 'AudioService',
      function (QuizService, QUIZ_CONFIG, $state, $timeout, AudioService) {

          if (QuizService.CurrentQuestionIndex == -1) {
              $state.transitionTo('menu', { arg: 'arg' });
          }
          
          var quiz = $state.params.quiz;
          var part = $state.params.part;

          var results = this;
          results.service = QuizService;        

          results.gotoMenu = function () {
              AudioService.Click();
              $timeout(function () {
                  //$state.transitionTo('menu', { arg: 'arg' });
                  $state.transitionTo('menu', { quiz: quiz, part: part });
              }, 300);

          };

          $("html, body").animate({ scrollTop: 0 }, 200);

      }]);

