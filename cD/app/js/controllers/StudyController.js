﻿angular.module('app')
  .controller('StudyCtrl', ['$scope', 'QuizService', '$state', 'AudioService', '$timeout', 'QUIZ_CONFIG', '$stateParams',
      function ($scope, QuizService, $state, AudioService, $timeout, QUIZ_CONFIG, $stateParams) {
          
          var study = this;
          study.loading = true;

          var quiz  = $state.params.quiz;
          var part = $state.params.part;                    

          QuizService.InitializeStudyQuiz(quiz, part); //not async          

          if (!quiz) { //quiz not found
              alert("This quiz was not found or has no questions");
              $state.transitionTo('menu');
              return;
          }

          study.question = {};
          study.selectedChoices = [];
          study.questionCount = QuizService.QuestionCount;
          study.currentQuestion = 1;
          study.quizStarted = false;
          study.selectedChoices = [];          
          study.quizComplete = false;
          study.nextQuestion = false;          
          study.complete = false;
          study.countupText = "";

          /*#region count up timer */
          study.countup = false;
          var seconds = 0;
          var runTimer = true;
          var msgShown = false
          var timerinstance;
          var timer = function () {
              timerinstance = $timeout(function () {

                  if (runTimer) {

                      seconds++;
                     
                      //if (seconds > 60) {
                          study.countup = true;
                          study.countupText = moment.duration(seconds, "seconds").humanize();

                          $("#countup").fadeIn("slow");
                      //}
                     
                          var messagemins = QUIZ_CONFIG.STUDYMESSAGE;

                      if (seconds == (messagemins * 60) && !msgShown) {
                          toastr.success('Great work, keep it up!', { positionClass: "toast-top-center" });
                      }
                      
                      timer();
                  }

              }, 1000, true);
          };

          $scope.$on("$destroy", function (event) {
                  $timeout.cancel(timerinstance);
              }
          );
          var show

          /*#endregion*/

          var createQuestion = function () {

              study.quizStarted = true;
              study.quizComplete = false;
              study.nextQuestion = false;

              study.question = QuizService.GetNextQuestion();
              study.questionCount = QuizService.QuestionCount;
              study.currentQuestion = QuizService.CurrentQuestionIndex + 1;
              study.question.id = study.currentQuestion;

              study.selectedChoices = [];

              angular.forEach(study.question.choices, function (choice, index) {
                  var c = {
                      id: index,
                      text: choice,
                      selected: false,
                      correct: false,
                      incorrect: false
                  };
                  
                  c.correct = _.find(study.question.correct, function (answer, index) {
                      return answer == c.text;
                  });                  

                  this.push(c);

              }, study.selectedChoices);

              setQuestionIsMultipleChoice();              

          };

          var setQuestionIsMultipleChoice = function () {

              study.question.isMultiChoice = study.question.choices.length > 0;
              return study.question.isMultiChoice;
          }

          study.gotoNextQuestion = function () {
              $("html, body").animate({ scrollTop: 0 }, 200);
              AudioService.Click();

              if (study.currentQuestion == (study.questionCount - 1)) {
                  $timeout.cancel(timerinstance);

                  $timeout(function () {
                      AudioService.Tada();
                  }, 500);
                  
              }

              if (study.currentQuestion == study.questionCount) {
                  study.complete = true;                  
                  //$scope.$emit('quiz-complete');
              }
              else {
                  
                  $timeout(function () {
                      createQuestion();
                  }, 350);
              }             
              
          };
          
          study.finishStudy = function () {

              AudioService.Click();

              $timeout(function () {
                  $state.transitionTo('menu', { quiz: quiz, part: part });
              }, 250);
              
          };

          createQuestion();
          timer();
          study.loading = false;

          


      }]);

