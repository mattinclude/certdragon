angular.module('app')
  .controller('MenuCtrl', ['$scope', '$state', '$timeout', 'store', 'AudioService', 'QuizService', 'FileService','$rootScope',
function ($scope, $state, $timeout, store, AudioService, QuizService, FileService, $rootScope) {

          var menu = this;
          menu.viewIndex = 0;

          var splash = store.get("splash");
          if (splash !== true) {
              $scope.$emit('show-splash', true);
              store.set("splash", true);
          }

          menu.musicplaying = store.get("playmusic");
          if (menu.musicplaying == null || menu.musicplaying == undefined) {
              menu.musicplaying = true;
              store.set("playmusic", true);
          }

          if (menu.musicplaying == true) {
              AudioService.StartBgMusic();
          }

          menu.gotoObjectives = function () {

              AudioService.Click();
              AudioService.StopBgMusic();

              $timeout(function () {
                  $state.transitionTo('objectives', { quiz: menu.selectedQuiz, part: menu.selectedQuizPart });
              }, 250);

          };

          menu.gotoStudy = function () {

              AudioService.Click();
              AudioService.StopBgMusic();

              $timeout(function () {
                  $state.transitionTo('study', { quiz: menu.selectedQuiz, part: menu.selectedQuizPart });
              }, 250);

          };

          menu.startQuiz = function () {

              AudioService.Click();
              AudioService.StopBgMusic();

              $timeout(function () {
                  $state.transitionTo('quiz', { quiz: menu.selectedQuiz, part: menu.selectedQuizPart });
              }, 250);

          };

          menu.playMusic = function () {

              AudioService.Click();

              menu.musicplaying = true;
              store.set("playmusic", true);
              AudioService.StartBgMusic();
          };

          menu.stopMusic = function () {

              AudioService.Click();

              menu.musicplaying = false;
              store.set("playmusic", false);
              AudioService.StopBgMusic();
          };

          menu.showSplash = function () {
              $scope.$emit('show-splash', true);
          };


          menu.reloading = false;
          menu.reloadApp = function () {

              AudioService.Click();

              menu.reloading = true;

              $("#body").fadeOut("slow", function () {
                  store.remove("splash");
                  store.remove("playmusic");
                  document.location.reload();
              });

          };

          menu.stateEnum = Object.freeze({
              quizzes: {},
              parts: {},
              quizMenu: {}
          });

          /* new config */
          menu.loading = true;
          menu.loadingComplete = false;

          menu.quizlist = [];
          $rootScope.$on('fileload', function (event, args) {
              menu.loading = args.loading;
          });

          $rootScope.$on('fileload-complete', function (event) {
              $timeout(function () {
                  menu.loadingComplete = true;
              }, 1000);
          });

          FileService.QuizFiles.GetExamFileObjects();
          menu.quizlist = FileService.QuizFileObjects;

          /**/

          menu.selectedQuiz = null;
          menu.selectedQuizPart = null;

          var quiz = $state.params.quiz;
          var part = $state.params.part;

          if (quiz !== undefined && quiz !== null && part !== undefined && part !== null) {
              menu.selectedQuiz = quiz; //QuizService.GetQuiz(qid);
              menu.selectedQuizPart = part; //QuizService.GetPart(qid, pid);
              menu.viewIndex = 2;
          }
          else if (quiz !== undefined && quiz !== null) {
              menu.selectedQuiz = quiz; //QuizService.GetQuiz(qid);
              menu.selectedQuizPart = null;
              menu.viewIndex = 1;
          }
          else {
              menu.viewIndex = 0;
          }

          menu.selectQuiz = function (quiz) {
              AudioService.Click();
              menu.selectedQuiz = quiz;

              menu.viewIndex = 1;
              $("html, body").animate({ scrollTop: 0 }, 200);
          };

          menu.selectQuizPart = function (part) {
              AudioService.Click();
              menu.selectedQuizPart = part;
              $scope.$emit('selected-quiz', { quiz: menu.selectedQuiz, part: menu.selectedQuizPart }); //for MainCtrl
              menu.viewIndex = 2;
          };

          menu.back = function (viewIndex) {
              AudioService.Click();
              menu.viewIndex = viewIndex;
          };

          menu.shakeme = false;
          menu.showpartsdesc = true;
          menu.toggleShake = function () {
              menu.shakeme = !menu.shakeme;
          };

          $("html, body").animate({ scrollTop: 0 }, 200);

          menu.toggleShake();

      }]);
