﻿app.filter('escape', function () {
    return window.encodeURIComponent;
});
app.filter('unescape', function () {
    return window.decodeURIComponent;
});