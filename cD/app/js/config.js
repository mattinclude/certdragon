﻿'use strict';

app.constant("QUIZ_CONFIG", {
    "ROOT":"/",
    "TEMPLATEPATH": "app/html/templates/",
    "QUIZTITLE":"certDragon",
    "QUIZLENGTH": 2, //number of minutes
    "QUESTIONCOUNT": 5, //number of questions to pick from pool
    "MAXSELECTIONS": 4,  //max number of questions user can select in a question,
    "STUDYMESSAGE": 2, //number of mins of studying before showing message.
    "SPLASHDURATION": 3 //number of seconds to show splash page.
});
