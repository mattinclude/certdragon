﻿'use strict';

// Declares how the application should be bootstrapped. See: http://docs.angularjs.org/guide/module
var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'angular-storage'])

    // Gets executed during the provider registrations and configuration phase. Only providers and constants can be
    // injected here. This is to prevent accidental instantiation of services before they have been fully configured.
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$tooltipProvider', 'QUIZ_CONFIG', '$sceProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $tooltipProvider, QUIZ_CONFIG, $sceProvider) {

            //disable html rendering interference
            $sceProvider.enabled(false);

            // UI States, URL Routing & Mapping. For more info see: https://github.com/angular-ui/ui-router
            // ------------------------------------------------------------------------------------------------------------

            // For any unmatched url, redirect to /
            $urlRouterProvider.otherwise("/");
            var templatePath = QUIZ_CONFIG.ROOT + QUIZ_CONFIG.TEMPLATEPATH;
            var root = QUIZ_CONFIG.ROOT;

            $stateProvider
                .state('menu', {
                    url: root,
                    templateUrl: templatePath + 'menu.html',
                    controller: 'MenuCtrl',
                    controllerAs: 'menu',
                    params: {
                        'quiz': null,
                        'part': null
                    },
                    data: {
                        title: 'certDragon',
                        showTitle: false,
                        showCountdown: false,
                        showBgImage: true
                    }

                })
                .state('objectives', {
                    url: root + 'objectives',
                    templateUrl: templatePath + 'objectives.html',
                    controller: 'ObjectivesCtrl',
                    controllerAs: 'obj',
                    params: {
                        'quiz': null,
                        'part': null
                    },
                    data: {
                        title: 'Exam Objectives'
                    }
                })
                .state('study', {
                    url: root + 'study-questions',
                    templateUrl: templatePath + 'study-questions.html',
                    controller: 'StudyCtrl',
                    controllerAs: 'study',
                    params: {
                        'quiz': null,
                        'part': null
                    },
                    data: {
                        title: 'Study Questions'
                    }

                })
                .state('quiz', {
                    url: root + 'quiz',
                    templateUrl: templatePath + 'quiz.html',
                    controller: 'QuizCtrl',
                    controllerAs: 'quiz',
                    params: {
                        'quiz': null,
                        'part': null
                    },
                    data: {
                        title: 'certDragon'
                    }

                })
                .state('results', {
                    url: root + 'results',
                    templateUrl: templatePath + 'results.html',
                    controller: 'ResultsCtrl',
                    controllerAs: 'results',
                    params: {
                        'quiz': null,
                        'part': null
                    },
                    data: {
                        title: 'Quiz Results'
                    }

                })
                .state('editor', {
                    url: root + 'editor',
                    templateUrl: templatePath + 'data-editor.html',
                    controller: 'EditorCtrl',
                    controllerAs: 'editor',
                    data: {
                        title: 'Quiz Data Editor'
                    }

                })
                .state('otherwise', {
                    url: '*path',
                    templateUrl: templatePath + '404.html',
                    controller: 'ErrorCtrl',
                    controllerAs: 'err',
                    data: {
                        title: '404'
                    }
                });

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            //workaround for safari bug
            $tooltipProvider.setTriggers({ 'click focus': 'mouseleave blur' });

        }])
    // Gets executed after the injector is created and are used to kickstart the application. Only instances and constants
    // can be injected here. This is to prevent further system configuration during application run time.
    .run(['$templateCache', '$rootScope', '$state', '$stateParams', 'QuizService', 'QUIZ_CONFIG',
        function ($templateCache, $rootScope, $state, $stateParams, QuizService, QUIZ_CONFIG) {

            // <ui-view> contains a pre-rendered template for the current view
            // caching it will prevent a round-trip to a server at the first page load
            var view = angular.element('#ui-view');
            $templateCache.put(view.data('tmpl-url'), view.html());

            // Allows to retrieve UI Router state information from inside templates
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            $rootScope.$on('$stateChangeSuccess', function (event, toState) {

                // Sets the layout name, which can be used to display different layouts (header, footer etc.)
                // based on which page the user is located
                $rootScope.layout = toState.layout;
            });

            //scroll to top of page on route change
            $("html, body").animate({ scrollTop: 0 }, 200);

            //preload images
            ; (function ($) {
                $.preload = function () {
                    var tmp = [], i = arguments.length;
                    // reverse loop run faster
                    for (; i-- ;) tmp.push($('<img />').attr('src', arguments[i]));
                };
            })(jQuery);

            $.preload(QUIZ_CONFIG.ROOT + 'images/Graduation-Students2.jpg', QUIZ_CONFIG.ROOT + 'images/quiz.jpg');

        }]);
