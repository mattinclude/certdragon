'use strict';

app.factory('FileService', ['$http', '$filter', '$cacheFactory', '$q', '$timeout', '$rootScope', 'QUIZ_CONFIG',
    function ($http, $filter, $cacheFactory, $q, $timeout, $rootScope, QUIZ_CONFIG) {

    var svc = {};

    //#region ENTITIES
    svc.QuizFiles = {};
    svc.Debug = true;
    //#endregion

    var api = function (req) {

        return $http(req).then(function (returnValue, status, headers, config) {
            return returnValue;
        }, function (returnValue, status, headers, config) {
            return returnValue;
        });

    };

    //#region QuizFiles

    svc.QuizFileObjects = [];
    var filenames = [];
    var filefolder = 'data/exams/';

    svc.QuizFiles.GetExamFiles = function () {

        var req = {
            method: 'GET',
            url: QUIZ_CONFIG.ROOT + filefolder
        };

        return api(req);

    };

    var timeout = 1000;

    svc.QuizFiles.GetExamFileObjects = function () {

        $rootScope.$emit('fileload', { loading: true });

        filenames = [];
        svc.QuizFileObjects = [];

        function getFiles() {
            var deferred = $q.defer();
            deferred.promise
            .then(function () {

                svc.QuizFiles.GetExamFiles().then(function (response) {

                    if (svc.Debug)
                        console.log("GetExamFiles response: ", response);

                    if (response.status === 200) {

                        $(response.data).find("a").each(function () {

                            var href = $(this).attr("href");
                            if (href.indexOf(".js") > -1) {

                                if (href.indexOf(filefolder) > -1) {
                                    filenames.push(href);
                                }
                                else {
                                    filenames.push(QUIZ_CONFIG.ROOT + filefolder + href);
                                }

                                if(svc.Debug)
                                    console.log("data file found: ", href);
                            }

                        });

                    }
                    else {
                        alert('error loading files! - ' + response.toString());
                        console.error("error loading files!", response);

                    }

                }).finally(function () {
                    if (svc.Debug)
                        console.log("data files loaded: ", filenames.length);
                });

            });
            $timeout(function () {
                deferred.resolve();
            }, timeout);
            return deferred.promise;
        }

        function getFileContents() {

            var deferred = $q.defer();
            var promise = deferred.promise
            .then(function () {

                if (svc.Debug)
                    console.log("loading file array: ", filenames);

                angular.forEach(filenames, function (filename) {

                    if (svc.Debug)
                        console.log("loading file: ", filename);

                    svc.QuizFiles.GetExamFile(filename).then(function (response) {

                        if (response.status === 200) {
                            response.data.filepath = filename;
                            svc.QuizFileObjects.push(response.data);

                            if (svc.Debug)
                                console.log("QuizFileObjects: ", svc.QuizFileObjects);

                        }
                        else {
                            alert('error loading file! - ' + response.toString());
                            console.error("error loading file!", response);
                        }

                    }).finally(function () {

                    });

                });

            });

            $timeout(function () {
                deferred.resolve();
            }, timeout);
            return promise;

        };

        var deferred = $q.defer();
        deferred.promise
        .then(function () {
            return getFiles();
        })
        .then(function () {
            var promise = getFileContents();

            $rootScope.$emit('fileload', { loading: false });
            $rootScope.$emit('fileload-complete');
            //console.log("loading done");

            return promise;
        });

        deferred.resolve();
    };

    svc.QuizFiles.GetExamFile = function (filename) {

        $cacheFactory.get('$http').remove(filename);

        var req = {
            method: 'GET',
            url: filename
        };

        return api(req);

    };

    svc.QuizFiles.SaveExamFile = function (filepath, data) {

        var fileparts = filepath.split("/");
        var filename = fileparts[fileparts.length - 1];

        filename = $filter('unescape')(filename);

        if (!data) {
            console.error('No data');
            return;
        }

        if (!filename) {
            filename = 'download.json';
        }

        if (typeof data === 'object') {
            data = JSON.stringify(data, undefined, 2);
        }

        var blob = new Blob([data], { type: 'text/json' }),
          e = document.createEvent('MouseEvents'),
          a = document.createElement('a');

        a.download = filename;
        a.href = window.URL.createObjectURL(blob);
        a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
        e.initMouseEvent('click', true, false, window,
            0, 0, 0, 0, 0, false, false, false, false, 0, null);
        a.dispatchEvent(e);

    };

    //#endregion

    return svc;

}]);
