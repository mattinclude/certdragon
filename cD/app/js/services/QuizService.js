﻿'use strict';

app.factory('QuizService', ['QUIZ_CONFIG', function (QUIZ_CONFIG) {

    var svc = {};

    //svc.Quizzes = [];

    svc.Questions = [];
    svc.MissedQuestions = [];
    svc.QuestionPool = [];
    svc.QuestionCount = 0;
    svc.CurrentQuestion = {};
    svc.CurrentQuestionIndex = -1;
    svc.Score = 0;
    svc.QuizRunning = false;
    svc.Exists = false;

    /* obsolete
    //svc.GetQuizzes = function () {
        
    //    if (svc.Quizzes.length == 0) {
    //        svc.Quizzes = QuestionService.GetQuizzes();
    //    }
       
    //    return svc.Quizzes;
    //};
    */

    svc.InitializeQuiz = function (quiz, part) {

        svc.Questions = [];        
        svc.Exists = false;

        var questions = part.questions; //QuestionService.GetQuestions(quizId, partId);

        if (questions == undefined) {
            return null;
        }

        svc.Exists = true;
        svc.QuestionPool = questions;
        selectQuestionsFromPool(svc.QuestionPool);        

        svc.MissedQuestions = [];
        svc.CurrentQuestion = {};
        svc.CurrentQuestionIndex = -1;
        svc.Score = 0;
        svc.QuizRunning = true;
        //console.log(svc.Questions);
    }

    svc.InitializeStudyQuiz = function (quiz, part) {

        svc.Questions = [];
        svc.Exists = false;
        console.log("InitializeStudyQuiz part =>", part);

        var questions = part.questions; //QuestionService.GetQuestions(quizId, partId);
        if (questions == undefined) {
            return null;
        }

        svc.Exists = true;
        svc.QuestionPool = questions;
        randomizeAllQuestions(svc.QuestionPool);

        svc.MissedQuestions = [];
        svc.CurrentQuestion = {};
        svc.CurrentQuestionIndex = -1;
        svc.Score = 0;
        svc.QuizRunning = true;
        //console.log(svc.Questions);
    };

    /* obsolete
    svc.InitializeStudyQuiz = function (quizId, partId) {

        svc.Questions = [];
        svc.Exists = false;

        var questions = QuestionService.GetQuestions(quizId, partId);
        if (questions == undefined) {
            return null;
        }

        svc.Exists = true;
        svc.QuestionPool = questions.questions;
        randomizeAllQuestions(svc.QuestionPool);

        svc.MissedQuestions = [];
        svc.CurrentQuestion = {};
        svc.CurrentQuestionIndex = -1;
        svc.Score = 0;
        svc.QuizRunning = true;
    };
    */

    svc.GetNextQuestion = function () {

        svc.CurrentQuestionIndex++;
        svc.CurrentQuestion = svc.Questions[svc.CurrentQuestionIndex];

        return svc.CurrentQuestion;
    };

    svc.GetPercentage = function () {
        return Math.round(svc.Score / svc.Questions.length * 100);
    };

    var selectQuestionsFromPool = function (pool) {

        if (QUIZ_CONFIG.QUESTIONCOUNT > pool.length) {
            console.warn("There are less questions than the quiz length specified!");
            console.warn("questioncount is", QUIZ_CONFIG.QUESTIONCOUNT);
            console.warn("available questions", pool.length);
            svc.QuestionCount = pool.length; //set quiz to available number of questions
        }
        else {
            svc.QuestionCount = QUIZ_CONFIG.QUESTIONCOUNT;
        }

        var arr = [];
        while (arr.length < svc.QuestionCount) {

            var randomnumber = Math.ceil(Math.random() * pool.length);
            randomnumber--; //to convert to index related

            var found = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == randomnumber) { found = true; break }
            }
            if (!found) {
                arr[arr.length] = randomnumber;

                //randomize choice order
                if (pool[randomnumber].choices.length > 1) {
                    pool[randomnumber].choices = getRandomizedArray(pool[randomnumber].choices);
                }
                
                svc.Questions.push(pool[randomnumber]);
            }
        }

        return arr;

    };

    var randomizeAllQuestions = function (pool) {

        svc.QuestionCount = pool.length;

        var arr = [];
        while (arr.length < svc.QuestionCount) {

            var randomnumber = Math.ceil(Math.random() * pool.length);
            randomnumber--; //to convert to index related

            var found = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == randomnumber) { found = true; break }
            }
            if (!found) {
                arr[arr.length] = randomnumber;

                //randomize choice order
                if (pool[randomnumber].choices.length > 1) {
                    pool[randomnumber].choices = getRandomizedArray(pool[randomnumber].choices);
                }

                svc.Questions.push(pool[randomnumber]);
            }
        }

        return arr;

    };

    var getRandomizedArray = function (array) {

        var arr = [];
        var randomArray = [];

        while (arr.length < array.length) {

            var randomnumber = Math.ceil(Math.random() * array.length);
            randomnumber--; //to convert to index related

            var found = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == randomnumber) { found = true; break }
            }
            if (!found) {
                arr[arr.length] = randomnumber;
                randomArray.push(array[randomnumber]);
            }
        }

        return randomArray;

    };

    /* obsolete
    svc.GetQuiz = function (quizId) {
        var quizzes = svc.GetQuizzes();
        var quiz = _.find(quizzes, function (q) {
            return q.id == quizId;
        });
        return quiz
    };

    svc.GetPart = function (quizId, partId) {
        var quiz = svc.GetQuiz(quizId);
        var part = _.find(quiz.parts, function (p) {
            return p.id == partId;
        });
        return part
    };

    svc.PartExists = function (quizId, partId) {
        var quiz = svc.GetQuiz(quizId);
        var part = _.find(quiz.parts, function (p) {
            return p.id == partId;
        });
        return part !== null && part !== undefined;
    };
    */

    return svc;

}]);

