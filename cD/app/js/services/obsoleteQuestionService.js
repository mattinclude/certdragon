﻿'use strict';

app.factory('QuestionService', [function () {

    var svc = {};

    svc.GetQuestions = function (quizId, partId) {

        return _.find(quizQuestions, function (qq) {
            return qq.quizId == quizId && qq.partId == partId;
        });
    };

    svc.GetQuizzes = function () {

        var quizzes = [
            {
                id: "quiz1",
                name: "Quiz 1",
                partstext: "This quiz is divided into 2 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    }
                ]
            },
            {
                id: "quiz2",
                name: "Quiz 2",
                partstext: "This quiz is divided into 2 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    }
                ]
            },
            {
                id: "quiz3",
                name: "Quiz 3",
                partstext: "This quiz is divided into 3 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    },
                    {
                        id: "part3",
                        name: "Part 3"
                    }

                ]
            },
            {
                id: "quiz4",
                name: "Quiz 4",
                partstext: "This quiz is divided into 2 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    }
                ]
            },
            {
                id: "quiz5",
                name: "Quiz 5",
                partstext: "This quiz is divided into 2 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    }
                ]
            },
            {
                id: "quiz6",
                name: "Quiz 6",
                partstext: "This quiz is divided into 2 parts. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    },
                    {
                        id: "part2",
                        name: "Part 2"
                    }
                ]
            },
            {
                id: "quiz7",
                name: "Quiz 7",
                partstext: "This quiz has only 1 part. Curabitur sed mauris odio. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sem diam, bibendum nec rutrum eget, tempor vel purus.",
                parts: [
                    {
                        id: "part1",
                        name: "Part 1"
                    }
                ]
            }
        ];

        return quizzes;
    };

    //questions are stored here:

    var quizQuestions = [
        {
            quizId: "quiz1", //quizId and partID *MUST* refer to a quiz in the GetQuizzes function
            partId: "part1",
            questions: [
                {
                    "question": "Which two people came up with the theory of relativity?",
                    "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Albert_Einstein_Head.jpg/220px-Albert_Einstein_Head.jpg",
                    "choices": [
                                    "Sir Isaac Newton",
                                    "Nicolaus Copernicus",
                                    "Albert Einstein",
                                    "Ralph Waldo Emmerson",
                                    "Ken Tilley"
                    ],
                    //NOTE: correct answers are now an array to allow for multiple selections by user. Format is always 
                    //[ "answer 1", "answer 2" ]
                    "correct": [
                        "Albert Einstein",
                        "Ken Tilley"
                    ],
                    "explanation": "Albert Einstein and Ken Tilley drafted the special theory of relativity in 1905.",
                },
                {
                    "question": "Who came up with the theory of relativity?",
                    "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Albert_Einstein_Head.jpg/220px-Albert_Einstein_Head.jpg",
                    "choices": [
                                        "Sir Isaac Newton",
                                        "Nicolaus Copernicus",
                                        "Albert Einstein",
                                        "Ralph Waldo Emmerson",
                                        "Ken Tilley"
                    ],
                    "correct": [
                        "Albert Einstein"
                    ],
                    "explanation": "Albert Einstein drafted the special theory of relativity in 1905.",
                },
                {
                    "question": "Who is on the two dollar bill?",
                    "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/9/94/US_%242_obverse-high.jpg/320px-US_%242_obverse-high.jpg",
                    "choices": [
                                        "Thomas Jefferson",
                                        "Dwight D. Eisenhower",
                                        "Benjamin Franklin",
                                        "Abraham Lincoln",
                                        "Ken Tilley"
                    ],
                    "correct": [
                        "Thomas Jefferson"
                    ],
                    "explanation": "The two dollar bill is seldom seen in circulation. As a result, some businesses are confused when presented with the note.",
                },
                {
                    "question": "What event began on April 12, 1861?",
                    "image": "",
                    "choices": [
                                        "First manned flight",
                                        "California became a state",
                                        "American Civil War began",
                                        "Declaration of Independence",
                                        "Ken Tilley"
                    ],
                    "correct": [
                        "American Civil War began"
                    ],
                    "explanation": "South Carolina came under attack when Confederate soldiers attacked Fort Sumter. The war lasted until April 9th 1865.",
                },
                {
                    "question": "Which of the following are even numbers?",
                    "image": "",
                    "choices": [
                                        "1",
                                        "2",
                                        "3",
                                        "4",
                                        "6"
                    ],
                    "correct": [
                        "2",
                        "4",
                        "6"
                    ],
                    "explanation": "2, 4 and 6 are even numbers",
                },
                {
                    "question": "What is the name of our planet?",
                    "image": "http://www.digitaljournal.com/img/8/7/3/i/3/9/8/o/Earth-from-Space.jpg",
                    "choices": [], //empty value will tell the app that the user will type their answer. 
                    "correct": [
                        "Earth" //NOTE: system will only match one answer - don't add multiples here
                    ],
                    "explanation": "The name of our planet is Earth",
                },
                {
                    "question": "What is the name of the this color?",
                    "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Red.svg/120px-Red.svg.png",
                    "choices": [], //empty value will tell the app that the user will type their answer. 
                    "correct": [
                        "red" //NOTE: system will only match one answer - don't add multiples here
                    ],
                    "explanation": "The color is red",
                }
            ]
        },
         {
             quizId: "quiz1",
             partId: "part2",
             questions: [
                 {
                     "question": "Which two people came up with the theory of relativity?",
                     "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Albert_Einstein_Head.jpg/220px-Albert_Einstein_Head.jpg",
                     "choices": [
                                     "Sir Isaac Newton",
                                     "Nicolaus Copernicus",
                                     "Albert Einstein",
                                     "Ralph Waldo Emmerson",
                                     "Ken Tilley"
                     ],
                     //NOTE: correct answers are now an array to allow for multiple selections by user. Format is always 
                     //[ "answer 1", "answer 2" ]
                     "correct": [
                         "Albert Einstein",
                         "Ken Tilley"
                     ],
                     "explanation": "Albert Einstein and Ken Tilley drafted the special theory of relativity in 1905.",
                 },
                 {
                     "question": "Who came up with the theory of relativity?",
                     "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Albert_Einstein_Head.jpg/220px-Albert_Einstein_Head.jpg",
                     "choices": [
                                         "Sir Isaac Newton",
                                         "Nicolaus Copernicus",
                                         "Albert Einstein",
                                         "Ralph Waldo Emmerson",
                                         "Ken Tilley"
                     ],
                     "correct": [
                         "Albert Einstein"
                     ],
                     "explanation": "Albert Einstein drafted the special theory of relativity in 1905.",
                 },
                 {
                     "question": "Who is on the two dollar bill?",
                     "image": "http://upload.wikimedia.org/wikipedia/commons/thumb/9/94/US_%242_obverse-high.jpg/320px-US_%242_obverse-high.jpg",
                     "choices": [
                                         "Thomas Jefferson",
                                         "Dwight D. Eisenhower",
                                         "Benjamin Franklin",
                                         "Abraham Lincoln",
                                         "Ken Tilley"
                     ],
                     "correct": [
                         "Thomas Jefferson"
                     ],
                     "explanation": "The two dollar bill is seldom seen in circulation. As a result, some businesses are confused when presented with the note.",
                 },
                 {
                     "question": "What event began on April 12, 1861?",
                     "image": "",
                     "choices": [
                                         "First manned flight",
                                         "California became a state",
                                         "American Civil War began",
                                         "Declaration of Independence",
                                         "Ken Tilley"
                     ],
                     "correct": [
                         "American Civil War began"
                     ],
                     "explanation": "South Carolina came under attack when Confederate soldiers attacked Fort Sumter. The war lasted until April 9th 1865.",
                 },
                 {
                     "question": "Which of the following are even numbers?",
                     "image": "",
                     "choices": [
                                         "1",
                                         "2",
                                         "3",
                                         "4",
                                         "6"
                     ],
                     "correct": [
                         "2",
                         "4",
                         "6"
                     ],
                     "explanation": "2, 4 and 6 are even numbers",
                 },
                 {
                     "question": "What is the name of our planet?",
                     "image": "http://www.digitaljournal.com/img/8/7/3/i/3/9/8/o/Earth-from-Space.jpg",
                     "choices": [], //empty value will tell the app that the user will type their answer. 
                     "correct": [
                         "Earth" //NOTE: system will only match one answer - don't add multiples here
                     ],
                     "explanation": "The name of our planet is Earth",
                 },
                 {
                     "question": "What is the name of the this color?",
                     "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Red.svg/120px-Red.svg.png",
                     "choices": [], //empty value will tell the app that the user will type their answer. 
                     "correct": [
                         "red" //NOTE: system will only match one answer - don't add multiples here
                     ],
                     "explanation": "The color is red",
                 }
             ]
         },
         {
             quizId: "quiz2",
             partId: "part1",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz2",
             partId: "part2",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz3",
             partId: "part1",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz3",
             partId: "part2",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz3",
             partId: "part3",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz4",
             partId: "part1",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz4",
             partId: "part2",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
        {
            quizId: "quiz5",
            partId: "part1",
            questions: [
                {
                    "question": "Question Text Here",
                    "image": "",
                    "choices": [
                                    "A",
                                    "B",
                                    "C"
                    ],
                    "correct": [
                        "A"
                    ],
                    "explanation": "This is the explanation",
                },
                {
                    "question": "Question Text Here",
                    "image": "",
                    "choices": [
                                    "A",
                                    "B",
                                    "C"
                    ],
                    "correct": [
                        "A"
                    ],
                    "explanation": "This is the explanation",
                },
                {
                    "question": "Question Text Here",
                    "image": "",
                    "choices": [
                                    "A",
                                    "B",
                                    "C"
                    ],
                    "correct": [
                        "A"
                    ],
                    "explanation": "This is the explanation",
                }
            ]
        },
         {
             quizId: "quiz5",
             partId: "part2",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz6",
             partId: "part1",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
         {
             quizId: "quiz6",
             partId: "part2",
             questions: [
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 },
                 {
                     "question": "Question Text Here",
                     "image": "",
                     "choices": [
                                     "A",
                                     "B",
                                     "C"
                     ],
                     "correct": [
                         "A"
                     ],
                     "explanation": "This is the explanation",
                 }
             ]
         },
          {
              quizId: "quiz7",
              partId: "part1",
              questions: [
                  {
                      "question": "Question Text Here",
                      "image": "",
                      "choices": [
                                      "A",
                                      "B",
                                      "C"
                      ],
                      "correct": [
                          "A"
                      ],
                      "explanation": "This is the explanation",
                  },
                  {
                      "question": "Question Text Here",
                      "image": "",
                      "choices": [
                                      "A",
                                      "B",
                                      "C"
                      ],
                      "correct": [
                          "A"
                      ],
                      "explanation": "This is the explanation",
                  },
                  {
                      "question": "Question Text Here",
                      "image": "",
                      "choices": [
                                      "A",
                                      "B",
                                      "C"
                      ],
                      "correct": [
                          "A"
                      ],
                      "explanation": "This is the explanation",
                  }
              ]
          },
    ];

    return svc;

}]);

