﻿'use strict';

app.factory('AudioService', [function () {

    var svc = {};    

    svc.audioPlaying = false;

    svc.Click = function () {
       
        var buttonclick = document.getElementById('buttonclick');
        buttonclick.play();

    };

    svc.Tada = function () {       
        
        var tada = document.getElementById('tada');
        tada.play();
    };
    
    svc.StartBgMusic = function () {

        if (svc.audioPlaying == false) {           

            var audio = document.getElementById('bgaudio');
            audio.play();
            svc.audioPlaying = true;
        }
        
    };

    svc.StopBgMusic = function () {
        svc.audioPlaying = false;
        var audio = document.getElementById('bgaudio');
        if (audio !== null && audio !== undefined) {
            audio.pause();
            audio.currentTime = 0;
        }       

    };

    return svc;

}]);

